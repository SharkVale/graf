#pragma once
#include "pch.h"
#include <queue>
using namespace std;

class graf
{
private:
	int glub;
	int v;
	int root;
	int sv;
	int **arr;
	int ways;
	int *status;
	queue <int> q;
public:
	graf();
	graf(int a);
	void addroot(int a);
	void showgraf();
	void nullgraf();
	void edges(int a, int b);
	void deep(int node);
	void breadth(int root);
	void CycloDyff(int n);
	void svshow();
};